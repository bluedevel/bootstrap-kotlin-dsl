package de.robinengel.bootstrap.dsl

enum class Level(private val level: String) {
    PRIMARY("primary"),
    SECONDARY("secondary"),
    SUCCESS("success"),
    DANGER("danger"),
    WARNING("warning"),
    INFO("info"),
    LIGHT("light"),
    DARK("dark");

    override fun toString(): String = level
}
