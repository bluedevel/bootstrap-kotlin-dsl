package de.robinengel.bootstrap.dsl

import kotlinx.html.*

enum class BadgeStyle(val cssClass: String) {
    DEFAULT(""),
    PILL("badge-pill");

    override fun toString(): String = cssClass
}

@HtmlTagMarker
fun FlowContent.badge(
    level: Level = Level.PRIMARY,
    style: BadgeStyle = BadgeStyle.DEFAULT,
    block: SPAN.() -> Unit = {}
) = span(classes = "badge badge-$level $style".trim(), block = block)

@HtmlTagMarker
fun FlowContent.badgeLink(
    level: Level = Level.PRIMARY,
    style: BadgeStyle = BadgeStyle.DEFAULT,
    href: String = "#",
    block: A.() -> Unit = {}
) = a(href = href, classes = "badge badge-$level $style".trim(), block = block)