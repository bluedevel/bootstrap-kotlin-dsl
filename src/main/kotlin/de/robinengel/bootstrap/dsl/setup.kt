package de.robinengel.bootstrap.dsl

import kotlinx.html.HtmlTagMarker
import kotlinx.html.MetaDataContent
import kotlinx.html.link

@HtmlTagMarker
fun MetaDataContent.bootstrapCss() =
    link(rel = "stylesheet", href = "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css")