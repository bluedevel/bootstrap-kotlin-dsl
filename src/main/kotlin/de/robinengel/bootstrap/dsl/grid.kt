package de.robinengel.bootstrap.dsl

import kotlinx.html.DIV
import kotlinx.html.FlowContent
import kotlinx.html.HtmlTagMarker
import kotlinx.html.div

@HtmlTagMarker
fun FlowContent.container(block: DIV.() -> Unit = {}) = div(classes = "container", block = block)

@HtmlTagMarker
fun FlowContent.row(block: DIV.() -> Unit = {}) = div(classes = "row", block = block)

enum class ColSize(val size: String) {
    EXTRA_SMALL(""),
    SMALL("-sm"),
    MEDIUM("-md"),
    LARGE("-lg"),
    EXTRA_LARGE("-xl");

    override fun toString(): String = size
}

interface IColWidth {
    fun toClass(): String
}

open class ColWidth(private val width: Int) : IColWidth {
    override fun toClass(): String {
        if (width == 0) {
            return ""
        } else {
            return "-$width"
        }
    }
}

class ColWidthNone : IColWidth {
    override fun toClass(): String = ""
}

class ColWidthAuto : IColWidth {
    override fun toClass(): String = "-auto"
}

class ColConf(val width: IColWidth = ColWidthNone(), val size: ColSize = ColSize.EXTRA_SMALL)

@HtmlTagMarker
fun FlowContent.col(
    vararg configs: ColConf,
    block: DIV.() -> Unit = {}
) {

    val configList = configs.asList().toMutableList()
    if (configList.isEmpty()) {
        configList += ColConf()
    }

    val classes = buildString {
        for (conf in configList) {
            append("col${conf.size}${conf.width.toClass()} ")
        }
    }

    div(classes = classes.trim(), block = block)
}