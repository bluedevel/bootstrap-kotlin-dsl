import kotlinx.html.HTML
import kotlinx.html.html
import kotlinx.html.stream.appendHTML
import org.junit.jupiter.api.Assertions

class HtmlAssertion(private var expected: String = "", private var actual: String = "") {

    companion object {
        fun assertEqualsHtml(block: HtmlAssertion.() -> Unit) {
            val htmlAssertion = HtmlAssertion()
            htmlAssertion.block()

            Assertions.assertEquals(htmlAssertion.expected, htmlAssertion.actual)
        }
    }

    fun expected(block: HTML.() -> Unit) {
        expected = buildString { appendHTML().html { block() } }
    }

    fun actual(block: HTML.() -> Unit) {
        actual = buildString { appendHTML().html { block() } }
    }
}