import de.robinengel.bootstrap.dsl.*
import kotlinx.html.body
import kotlinx.html.div
import kotlinx.html.p
import org.junit.jupiter.api.Test

class GridTests {

    @Test
    fun defaultGrid() = HtmlAssertion.assertEqualsHtml {
        expected {
            body {
                div(classes = "container") {
                    div(classes = "row") {
                        div(classes = "col") { p { +"Text 1" } }
                        div(classes = "col") { p { +"Text 2" } }
                        div(classes = "col") { p { +"Text 3" } }
                    }
                }
            }
        }

        actual {
            body {
                container {
                    row {
                        col { p { +"Text 1" } }
                        col { p { +"Text 2" } }
                        col { p { +"Text 3" } }
                    }
                }
            }
        }
    }

    @Test
    fun colWidth() = HtmlAssertion.assertEqualsHtml {
        expected {
            body {
                div(classes = "container") {
                    div(classes = "row") {
                        div(classes = "col-4") { p { +"Text 1" } }
                        div(classes = "col-3") { p { +"Text 2" } }
                        div(classes = "col-2") { p { +"Text 3" } }
                    }
                }
            }
        }

        actual {
            body {
                container {
                    row {
                        col(ColConf(ColWidth(4))) { p { +"Text 1" } }
                        col(ColConf(ColWidth(3))) { p { +"Text 2" } }
                        col(ColConf(ColWidth(2))) { p { +"Text 3" } }
                    }
                }
            }
        }
    }

    @Test
    fun colSize() = HtmlAssertion.assertEqualsHtml {
        expected {
            body {
                div(classes = "container") {
                    div(classes = "row") {
                        div(classes = "col-sm") { p { +"Text 1" } }
                        div(classes = "col-md") { p { +"Text 2" } }
                        div(classes = "col-xl") { p { +"Text 3" } }
                    }
                }
            }
        }

        actual {
            body {
                container {
                    row {
                        col(ColConf(size = ColSize.SMALL)) { p { +"Text 1" } }
                        col(ColConf(size = ColSize.MEDIUM)) { p { +"Text 2" } }
                        col(ColConf(size = ColSize.EXTRA_LARGE)) { p { +"Text 3" } }
                    }
                }
            }
        }
    }

    @Test
    fun colSizeAndWidth() = HtmlAssertion.assertEqualsHtml {
        expected {
            body {
                div(classes = "container") {
                    div(classes = "row") {
                        div(classes = "col-sm-2") { p { +"Text 1" } }
                        div(classes = "col-md-auto") { p { +"Text 2" } }
                        div(classes = "col-xl-6") { p { +"Text 3" } }
                    }
                }
            }
        }

        actual {
            body {
                container {
                    row {
                        col(ColConf(size = ColSize.SMALL, width = ColWidth(2))) { p { +"Text 1" } }
                        col(ColConf(size = ColSize.MEDIUM, width = ColWidthAuto())) { p { +"Text 2" } }
                        col(ColConf(size = ColSize.EXTRA_LARGE, width = ColWidth(6))) { p { +"Text 3" } }
                    }
                }
            }
        }
    }
}