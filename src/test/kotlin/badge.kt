import de.robinengel.bootstrap.dsl.BadgeStyle
import de.robinengel.bootstrap.dsl.Level
import de.robinengel.bootstrap.dsl.badge
import de.robinengel.bootstrap.dsl.badgeLink
import kotlinx.html.a
import kotlinx.html.body
import kotlinx.html.span
import org.junit.jupiter.api.Test

class BadgeTests {

    @Test
    fun defaultBadge() = HtmlAssertion.assertEqualsHtml {
        expected { body { span(classes = "badge badge-primary") { +"test text" } } }
        actual { body { badge { +"test text" } } }
    }

    @Test
    fun badgePill() = HtmlAssertion.assertEqualsHtml {
        expected { body { span(classes = "badge badge-primary badge-pill") { +"test text" } } }
        actual { body { badge(style = BadgeStyle.PILL) { +"test text" } } }
    }

    @Test
    fun badgeLink() = HtmlAssertion.assertEqualsHtml {
        expected { body { a(classes = "badge badge-primary", href = "link") { +"click here" } } }
        actual { body { badgeLink(href = "link") { +"click here" } } }
    }

    @Test
    fun badgeLinkPill() = HtmlAssertion.assertEqualsHtml {
        expected { body { a(classes = "badge badge-primary badge-pill", href = "link") { +"click here" } } }
        actual { body { badgeLink(href = "link", style = BadgeStyle.PILL) { +"click here" } } }
    }

    @Test
    fun badgeDanger() = HtmlAssertion.assertEqualsHtml {
        expected { body { span(classes = "badge badge-danger") { +"test text" } } }
        actual { body { badge(level = Level.DANGER) { +"test text" } } }
    }

    @Test
    fun badgeDangerPill() = HtmlAssertion.assertEqualsHtml {
        expected { body { span(classes = "badge badge-danger badge-pill") { +"test text" } } }
        actual { body { badge(style = BadgeStyle.PILL, level = Level.DANGER) { +"test text" } } }
    }

    @Test
    fun badgeLinkDanger() = HtmlAssertion.assertEqualsHtml {
        expected { body { a(classes = "badge badge-danger", href = "link") { +"click here" } } }
        actual { body { badgeLink(href = "link", level = Level.DANGER) { +"click here" } } }
    }

    @Test
    fun badgeLinkDangerPill() = HtmlAssertion.assertEqualsHtml {
        expected { body { a(classes = "badge badge-danger badge-pill", href = "link") { +"click here" } } }
        actual { body { badgeLink(href = "link", level = Level.DANGER, style = BadgeStyle.PILL) { +"click here" } } }
    }
}